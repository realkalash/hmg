public class Book extends Literature {

    protected String publisher;
    protected String autor;

    public Book(String name, String autor, String publisher, int year) {
        super(name, year);
        this.autor = autor;
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return "Book\n"
                + "Name: " + getName()
                + "\nAutor: " + getAutor()
                + "\nPublisher: " + getPublisher()
                + "\nYear: " + getYear();
    }

    public String getPublisher() {
        return publisher;
    }

    public String getAutor() {
        return autor;
    }
}

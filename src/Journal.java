public class Journal extends Literature {

    protected int month;
    protected String ganre;

    public Journal(String name, String ganre, int year, int month) {
        super(name, year);
        this.ganre = ganre;
        this.month = month;
    }

    @Override
    public String toString() {
        return String.format("Journal: %s %n" +
                "Ganre: %s %n" +
                "Year: %d%n" +
                "Month: %d%n", getName(), getGanre(), getYear(), getMonth());
    }

    public int getMonth() {
        return month;
    }

    public String getGanre() {
        return ganre;
    }

}

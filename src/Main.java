import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
//        Наследование EXTENDS это перенимание классом свойств другого класса

        Literature[] literatures = new Literature[6];
        literatures[0] = new Book("Война и мир", "Достоевский", "Солнышко", 1870);
        literatures[1] = new Book("Попаданцы", "Кошелёв", "Народные умельцы", 1870);
        literatures[2] = new Journal("Tesla news", "Наука", 2018, 8);
        literatures[3] = new Journal("Vogue", "Мода", 1988, 7);
        literatures[4] = new YearBook("Мои мысли круглый год", "Мысли", "Нет", 1870);
        literatures[5] = new YearBook("Ежегодник про чё-то", "Чё-то", "ОГДПУ", 2005);

        System.out.println("Введите год ");

        Scanner input = new Scanner(System.in);
        int inputStr = Integer.parseInt(input.nextLine());

        System.out.println("Поиск среди журналов: ");

        for (Literature literature : literatures) {
            if (literature instanceof Journal) {
                System.out.println(literature);
            }

            if (inputStr == literature.getYear()){
                System.out.println("По году: " + inputStr + " Было найдено: ");
                System.out.println(literature);
            }
        }
    }
}
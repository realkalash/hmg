public class YearBook extends Literature {

    protected String publisher;
    protected String ganre;


    public YearBook(String name, String ganre, String publisher, int year) {
        super(name, year);
        this.ganre = ganre;
        this.year = year;
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return "YearBook{" +
                "publisher='" + getPublisher() + '\'' +
                ", ganre='" + getGanre() + '\'' +
                ", name='" + getName() + '\'' +
                ", year=" + getYear() +
                '}';
    }

    public String getPublisher() {
        return publisher;
    }

    public String getGanre() {
        return ganre;
    }

}
